-- protected call to check if treesitter is properly installed and doesn't yield any error if it isn't
local status_ok, configs = pcall(require, "nvim-treesitter.configs")
if not status_ok then
  return
end

configs.setup {
  ensure_installed = "all", -- allow all parsers to be installed
  sync_install = false, -- install languages synchronously
  ignore_install = { "" }, -- List of parsers to ignore installing
  autopairs = {
    enable = true,
  },
  highlight = {
    enable = true, -- false will disable the whole extension
    disable = { "" }, -- list of language that will be disabled
    additional_vim_regex_highlighting = true,
  },
  -- allows the cursor to be indented at the place it should be indented on
  -- automatic cursor indentation doesn't work well with YAML so it is disabled
  indent = { enable = true, disable = { "yaml" } },
  -- enable treesitter rainbow for matching color paranthesis, curly braces...
  rainbow = {
    enable = true,
    -- disable = { "jsx", "cpp" }, list of languages you want to disable the plugin for
    extended_mode = true, -- Also highlight non-bracket delimiters like html tags, boolean or table: lang -> boolean
    max_file_lines = nil, -- Do not enable for files with more than n lines, int
    -- colors = {}, -- table of hex strings
    -- termcolors = {} -- table of colour name strings
  },
  -- treesitter config for context comment string to work
  context_commentstring = {
    enable = true,
    enable_autocmd = false,
  },
}

