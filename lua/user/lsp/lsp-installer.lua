-- protected call to check if nvom-lsp-installer is installed
local status_ok, lsp_installer = pcall(require, "nvim-lsp-installer")
if not status_ok then
	return
end

-- Register a handler that will be called for all installed servers.
-- Alternatively, you may also register handlers on specific server instances instead (see example below).
lsp_installer.on_server_ready(function(server)
  -- create variable holding all the options for each lsp
	local opts = {
		on_attach = require("user.lsp.handlers").on_attach,
		capabilities = require("user.lsp.handlers").capabilities,
	}

	 if server.name == "jsonls" then
	 	local jsonls_opts = require("user.lsp.settings.jsonls")
	 	opts = vim.tbl_deep_extend("force", jsonls_opts, opts)
	 end

	 if server.name == "sumneko_lua" then
	 	local sumneko_opts = require("user.lsp.settings.sumneko_lua")
	 	opts = vim.tbl_deep_extend("force", sumneko_opts, opts)
	 end

	 if server.name == "pyright" then
	 	local pyright_opts = require("user.lsp.settings.pyright")
	 	opts = vim.tbl_deep_extend("force", pyright_opts, opts)
	 end

  if server.name == "omnisharp" then
    local omnisharp_opts = require("user.lsp.settings.omnisharp")
    opts = vim.tbl_deep_extend("force", omnisharp_opts, opts)
  end

  if server.name == "clangd" then
    local clangd_opts = require("user.lsp.settings.clangd")
    opts = vim.tbl_deep_extend("force", clangd_opts, opts)
  end

  if server.name == "marksman" then
    local marksman_opts = require("user.lsp.settings.marksman")
    opts = vim.tbl_deep_extend("force", marksman_opts, opts)
  end

  if server.name == "eslint" then
    local eslint_opts = require("user.lsp.settings.eslint")
    opts = vim.tbl_deep_extend("force", eslint_opts, opts)
  end

  if server.name == "tsserver" then
    local tsserver_opts = require("user.lsp.settings.tsserver")
    opts = vim.tbl_deep_extend("force", tsserver_opts, opts)
  end

  if server.name == "solargraph" then
    local solargraph_opts = require("user.lsp.settings.solargraph")
    opts = vim.tbl_deep_extend("force", solargraph_opts, opts)
  end

  if server.name == "rust_analyzer" then
    local rust_analyzer_opts = require("user.lsp.settings.rust-analyzer")
    opts = vim.tbl_deep_extend("force", rust_analyzer_opts, opts)
  end

	-- This setup() function is exactly the same as lspconfig's setup function.
	-- Refer to https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md
  -- setup server with the local variable containing all the lsp options
	server:setup(opts)
end)
