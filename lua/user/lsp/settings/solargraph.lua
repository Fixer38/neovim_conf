return {
  cmd = { "solargraph", "stdio" },
  filetypes = { "ruby", "rakefile" },
  init_options = {
    formatting = true
  },
  settings = {
    solargraph = {
      diagnostics = true
    }
  }
}
