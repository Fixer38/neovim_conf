local pid = vim.fn.getpid()
local omnisharp_bin = "/home/fixer/.local/share/nvim/lsp_servers/omnisharp/omnisharp/OmniSharp"
return {
  cmd = { omnisharp_bin, "--languageserver" , "--hostPID", tostring(pid) };
}
