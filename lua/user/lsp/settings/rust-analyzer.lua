return {
  cmd = { "rust-analyzer" },
  filetypes = { "rust" },
  settings = {
        ['rust-analyzer'] = {
            checkOnSave = {
                lens = {
                  enable = true
                },
                allFeatures = true,
                overrideCommand = {
                    'cargo', 'clippy', '--workspace', '--message-format=json',
                    '--all-targets', '--all-features'
                }
            }
        }
    }
}
